using System;

namespace hashcode2018
{
    public class Point
    {
        private readonly int x;
        private readonly int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int X => x;
        public int Y => y;

        public int Distance(Point other) {
            return Math.Abs(other.x - this.x) + Math.Abs(other.y - this.y);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            Point other = (Point)obj;
            return other.x == this.x && other.y == this.y;
        }

        public override int GetHashCode()
        {
            return this.x * 31 + this.y;
        }
    }
}
