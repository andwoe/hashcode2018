using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace hashcode2018
{
    class Program
    {
        static void Main(string[] args)
        {
            var fileNames = new string[]{"a_example", "b_should_be_easy", "c_no_hurry", "d_metropolis", "e_high_bonus"};
            foreach (var fileName in fileNames) {
                ProcessFile(@"input/" + fileName + ".in", @"output/" + fileName + ".out");
            }
        }

        private static void ProcessFile(string inputFilePath, string outputFilePath)
        {
            var inputLines = File.ReadAllLines(inputFilePath).Select(x => x.Split(" ").Select(val => int.Parse(val)).ToArray()).ToArray();

            var R = inputLines[0][0];
            var C = inputLines[0][1];
            var F = inputLines[0][2];
            var N = inputLines[0][3];
            var B = inputLines[0][4];
            var T = inputLines[0][5];

            var rides = new List<Ride>();

            for (int lineIdx = 1; lineIdx <= N; lineIdx++)
            {
                var line = inputLines[lineIdx];
                //parse Intersection Start
                var startIntersection = new Point(line[0], line[1]);
                //parse Intersection End
                var endIntersection = new Point(line[2], line[3]);
                //parse Ride
                var ride = new Ride(startIntersection, endIntersection, line[4], line[5], lineIdx - 1);
                rides.Add(ride);
            }

            rides.Sort((Ride first, Ride second) => {
                return first.LatestFinish.CompareTo(second.LatestFinish);
            });

            var simulation = new Simulation(R, C, F, N, B, T, rides);
            var taxis = simulation.Run();

            Console.WriteLine(inputFilePath);
            using (var writer = new StreamWriter(outputFilePath))
            {
                foreach (var taxi in taxis)
                {
                    var outputLine = taxi.finishedRides.Count + " " + String.Join(" ", taxi.finishedRides.Select(x => x.ID));
                    writer.WriteLine(outputLine);
                    Console.WriteLine(outputLine);
                }
            }
            Console.WriteLine();
        }
    }
}
