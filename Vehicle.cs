using System.Collections.Generic;

namespace hashcode2018
{
    public class Vehicle
    {
        public Point Position {get; set;}
        public Ride Passenger {get; set;}

        public Point Target { get; set; }

        public List<Ride> finishedRides;

        public Vehicle(Point startPosition, Ride passenger = null) {
            Position = startPosition;
            Passenger = passenger;
            if(Passenger != null) {
                Target = Target == Passenger.StartPoint ? Passenger.FinishPoint : Passenger.StartPoint;
            }
            finishedRides = new List<Ride>();
        }

        public bool HasPassenger => Passenger != null;

        public int Distance (Point other) => Position.Distance(other);

        public void SetPassenger (Ride passenger) {
            if(!HasPassenger) {
                Passenger = passenger;
                if(Passenger != null) {
                    Target = Target == Passenger.StartPoint ? Passenger.FinishPoint : Passenger.StartPoint;
                }
            }
        }

        public void Move(int currentTime) {
            if(Target != null) {
                if(Target == Passenger.StartPoint) {
                    Position = moveTo();
                    if(Target == Passenger.StartPoint) {
                        Target = Passenger.FinishPoint;
                    }
                } else {
                    if(Passenger.EarliestStart <= currentTime) {
                        Position = moveTo();
                        if(Target == Passenger.FinishPoint) {
                            finishedRides.Add(Passenger);
                            Passenger = null;
                            Target = null;
                        }
                    }
                }
            }
        }

        private Point moveTo () {
            int x = Position.X;
            int y = Position.Y;

            if(x != Target.X) {
                if( x < Target.X)
                    x++;
                else 
                    x--;
            } 
            else {
                if( y < Target.Y)
                    y++;
                else 
                    y--;
            }
            return new Point(x, y);
        }
    }
}