using System;
using System.Collections.Generic;
using System.Linq;

namespace hashcode2018
{
    public class Simulation
    {
        public Simulation(int R, int C, int F, int N, int B, int T, List<Ride> rides) {
            this.R = R;
            this.C = C;
            this.F = F;
            this.N = N;
            this.B = B;
            this.T = T;
            this.Rides = rides;
        }

        public int R { get; }
        public int C { get; }
        public int F { get; }
        public int N { get; }
        public int B { get; }
        public int T { get; }
        public List<Ride> Rides { get; }
        private List<Ride> unhandledRides; 

        public List<Vehicle> Run() {
            var time = 0;

            var taxis = new List<Vehicle>();
            for (int i = 0; i < F; i++)
            {
                taxis.Add(new Vehicle(new Point(0, 0)));
            }

            unhandledRides = new List<Ride>(Rides);

            while(time < T && (unhandledRides.Count > 0 || taxis.Any(x => x.HasPassenger)))
            {
                CleanOld(ref unhandledRides, time);
                AssignTaxi(taxis, time, B);
                taxis.ForEach(t =>
                {
                    t.Move(time);
                });

                time++;
                if (unhandledRides.Count > 0 && taxis.Any(x => !x.HasPassenger))
                {
                    AssignTaxi(taxis, time, B);
                }
            }
            return taxis;
        }

        private void AssignTaxi(List<Vehicle> taxis, int time, int B)
        {
            var idleTaxis = taxis.Where(x => !x.HasPassenger).ToArray();
            foreach (var idle in idleTaxis)
            {
                if (unhandledRides.Count > 0)
                {
                    unhandledRides.Sort((first, second) => strategyCompare(first, second, idle, time, B));
                    var nextRide = unhandledRides.First();
                    unhandledRides.Remove(nextRide);
                    idle.SetPassenger(nextRide);
                }
            }
        }

        private void CleanOld (ref List<Ride> unhandledRides, int time) {
            unhandledRides.RemoveAll(r => time > r.LatestFinish || (r.StartPoint.Distance(r.FinishPoint) + time) > r.LatestFinish);
        }


        public int strategyCompare(Ride first, Ride second, Vehicle taxi, int time, int B) {
            int c = first.LatestFinish.CompareTo(second.LatestFinish);
            if (c != 0) {
                return c;
            }
            //return first.StartPoint.Distance(taxi.Position).CompareTo(second.StartPoint.Distance(taxi.Position));
            return first.Metric(taxi.Position, time, B).CompareTo(second.Metric(taxi.Position, time, B));
        }
    }
}