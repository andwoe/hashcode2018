namespace hashcode2018
{
    public class Ride
    {
        private readonly Point startPoint;
        private readonly Point finishPoint;

        private readonly int earliestStart;
        private readonly int latestFinish;

        public int ID { get; }

        public Ride(Point startPoint, Point finishPoint, int earliestStart, int latestFinish, int id) {
            this.startPoint = startPoint;
            this.finishPoint = finishPoint;
            this.earliestStart = earliestStart;
            this.latestFinish = latestFinish;
            this.ID = id;
        }

        public Point StartPoint => startPoint;

        public Point FinishPoint => finishPoint;

        public int EarliestStart => earliestStart;

        public int LatestFinish => latestFinish;

        public int Metric (Point taxiPos, int time, int B) {
            return StartPoint.Distance(taxiPos) * 1 + ((EarliestStart - time >= StartPoint.Distance(taxiPos)) ? B : 0);
        }
    }
}
